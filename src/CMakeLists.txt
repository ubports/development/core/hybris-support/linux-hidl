include_directories(include)

add_subdirectory(binderthreadstate)
add_subdirectory(libhwbinder)
add_subdirectory(libfmq)
add_subdirectory(hidl)
add_subdirectory(libhidl)
add_subdirectory(bionic_functions)
add_subdirectory(system_core)
add_subdirectory(hardware)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/linux-hidl)
